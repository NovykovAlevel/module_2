package stolbunov.roman.homework_15.bl.arenas;

public interface FightTurnCallback {
    public void publishTurn(stolbunov.roman.homework_15.data.model.history.FightTurn turn);
}

package stolbunov.roman.homework_15.ui.screens;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import stolbunov.roman.homework_15.R;
import stolbunov.roman.homework_15.bl.arenas.DuelArena;
import stolbunov.roman.homework_15.data.cash.RuntimeCash;
import stolbunov.roman.homework_15.data.model.fighters.ArenaFighter;
import stolbunov.roman.homework_15.data.model.fighters.FighterType;
import stolbunov.roman.homework_15.data.model.fighters.FightersFactory;
import stolbunov.roman.homework_15.data.model.healers.Healer;
import stolbunov.roman.homework_15.data.model.history.FightTurn;


public class FightActivity extends AppCompatActivity {


    private static final String FIGHTER_TYPE = "Fighter_Type";

    public static Intent createFighter(Context context, FighterType type) {
        Intent intent = new Intent(context, FightActivity.class);
        intent.putExtra(FIGHTER_TYPE, type.getType());
        return intent;
    }


    private ArenaFighter oponent;


    List<FightTurn> turns = new LinkedList<>();


    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    handleFightTurn((FightTurn) msg.obj);
                    break;
            }

        }

    };

    private void handleFightTurn(FightTurn turn) {
        turns.add(turn);
        LayoutInflater inflater = getLayoutInflater();
        TextView textView = (TextView) inflater.inflate(R.layout.view_fight_turn, null, false);
        content.addView(textView);
        textView.setText(turn.toString());
        //TODO inflate view and add to screen

        if (turn.isFinal()) {
            fightDone.setEnabled(true);
        }
    }


    ViewGroup content;
    View fightDone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fight);
        content = findViewById(R.id.content);
        fightDone = findViewById(R.id.btn_fightDone);

        fightDone.setEnabled(false);
        fightDone.setOnClickListener(this::fightComplete);
        //TODO read satate
        if (savedInstanceState == null) {
            oponent = FightersFactory.generateFighter(FighterType.KNIGHT);
        } else {
            //TODO restore
        }
        new Fight().start();
    }

    private void fightComplete(View view) {
        RuntimeCash cash = RuntimeCash.getInstance();
        int fightHistoryId = cash.puplicshFight(turns, oponent);
        Intent data = new Intent();
        data.putExtra("result", fightHistoryId);
        setResult(RESULT_OK, data);
        finish();
    }


    class Fight extends Thread {
        @Override
        public void run() {
            ArenaFighter current = RuntimeCash.getInstance().getCurrentFIghetr();
            DuelArena duelArena = new DuelArena(new Healer("Bishop", 50), current, oponent, 20);
            duelArena.setFightCalback(turn -> {
                handler.sendMessage(Message.obtain(handler, 1, turn));
            });
            duelArena.startBattle();
        }
    }

}

package stolbunov.roman.homework_15.ui.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;


import com.bumptech.glide.Glide;

import stolbunov.roman.homework_15.R;
import stolbunov.roman.homework_15.data.model.fighters.ArenaFighter;
import stolbunov.roman.homework_15.data.model.fighters.FighterType;
import stolbunov.roman.homework_15.data.model.fighters.FightersFactory;

public class ChooseFighterActivity extends AppCompatActivity {
    private static final String URI_DRAGON =
            "https://dragon-quest.org/images/thumb/4/4c/DQIII_Wyrtle.png/280px-DQIII_Wyrtle.png";
    private static final String URI_DRAGON_RIDER =
            "https://i.pinimg.com/originals/59/5e/c6/595ec61c7774c73ded78f8e89d353fa6.png";
    private static final String URI_KNIGHT =
            "https://dragon-quest.org/images/thumb/b/b4/DQV_Slime_Knight.png/250px-DQV_Slime_Knight.png";
    private static final String URI_HOLLY_KNIGHT =
            "https://cdn2.iconfinder.com/data/icons/fantasy-characters/512/knight2-512.png";
    private static final String URI_BLACK_KNIGHT =
            "https://cdn2.iconfinder.com/data/icons/fantasy-characters/512/skeleton2-512.png";
    private static final String URI_MAGE =
            "https://cdn2.iconfinder.com/data/icons/fantasy-characters/512/wizard3-512.png";

    private ImageButton imageButtonDragon;
    private ImageButton imageButtonDragonRider;
    private ImageButton imageButtonKnight;
    private ImageButton imageButtonHollyKnight;
    private ImageButton imageButtonBlackKnight;
    private ImageButton imageButtonMage;
    private ArenaFighter choosedFighter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_fighter);
        imageButtonDragon = findViewById(R.id.dragon_btn_icon);
        imageButtonDragonRider = findViewById(R.id.dragon_rider_btn_icon);
        imageButtonKnight = findViewById(R.id.knight_btn_icon);
        imageButtonHollyKnight = findViewById(R.id.holly_knight_btn_icon);
        imageButtonBlackKnight = findViewById(R.id.black_knight_btn_icon);
        imageButtonMage = findViewById(R.id.mage_btn_icon);

        imageButtonDragon.setOnClickListener(this::onclick);
        imageButtonDragonRider.setOnClickListener(this::onclick);
        imageButtonKnight.setOnClickListener(this::onclick);
        imageButtonHollyKnight.setOnClickListener(this::onclick);
        imageButtonBlackKnight.setOnClickListener(this::onclick);
        imageButtonMage.setOnClickListener(this::onclick);

        loadingImage(URI_DRAGON, imageButtonDragon);
        loadingImage(URI_DRAGON_RIDER, imageButtonDragonRider);
        loadingImage(URI_KNIGHT, imageButtonKnight);
        loadingImage(URI_HOLLY_KNIGHT, imageButtonHollyKnight);
        loadingImage(URI_BLACK_KNIGHT, imageButtonBlackKnight);
        loadingImage(URI_MAGE, imageButtonMage);

    }

    public void onclick(View v) {
        switch (v.getId()) {
            case R.id.dragon_btn_icon:
                choosedFighter = FightersFactory.generateFighter(FighterType.DRAGON);
                goToFighterCharacteristicsActivity();
                break;
            case R.id.dragon_rider_btn_icon:
                choosedFighter = FightersFactory.generateFighter(FighterType.DRAGON_RIDER);
                goToFighterCharacteristicsActivity();
                break;
            case R.id.knight_btn_icon:
                choosedFighter = FightersFactory.generateFighter(FighterType.KNIGHT);
                goToFighterCharacteristicsActivity();
                break;
            case R.id.holly_knight_btn_icon:
                choosedFighter = FightersFactory.generateFighter(FighterType.HOLLY_KNIGHT);
                goToFighterCharacteristicsActivity();
                break;
            case R.id.black_knight_btn_icon:
                choosedFighter = FightersFactory.generateFighter(FighterType.BLACK_KNIGHT);
                goToFighterCharacteristicsActivity();
                break;
            case R.id.mage_btn_icon:
                choosedFighter = FightersFactory.generateFighter(FighterType.MAGE);
                goToFighterCharacteristicsActivity();
                break;
            default:
                break;
        }
    }


    private void loadingImage(String uri, ImageView view) {
        Glide.with(this)
                .load(uri)
                .into(view);
    }

    private void goToFighterCharacteristicsActivity() {
        Intent intentLoadChooseFighterActivity = new Intent(ChooseFighterActivity.this,
                FighterCharacteristicsActivity.class);
        intentLoadChooseFighterActivity.putExtra(ArenaFighter.class.getCanonicalName(),choosedFighter);
        startActivity(intentLoadChooseFighterActivity);
    }

}

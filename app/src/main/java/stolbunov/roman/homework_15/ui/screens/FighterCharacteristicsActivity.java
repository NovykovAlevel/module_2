package stolbunov.roman.homework_15.ui.screens;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import stolbunov.roman.homework_15.R;
import stolbunov.roman.homework_15.data.model.fighters.ArenaFighter;
import stolbunov.roman.homework_15.data.model.fighters.FightersFactory;

public class FighterCharacteristicsActivity extends AppCompatActivity {
    private LinearLayout fighterProfile;
    private View profileSections;

    private ImageView profileImage;

    private TextView profileName;
    private TextView profileDescription;

    private TextView currentHp;
    private TextView currentArmor;
    private TextView currentDamage;

    private Button reCreate;
    private Button select;

    private View fighterProfileView;
    private ArenaFighter choosedFighter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fighter_characteristics);

        LayoutInflater ltInflater = getLayoutInflater();
        FrameLayout frLayout = findViewById(R.id.fl_profile);
        fighterProfileView = ltInflater.inflate(R.layout.view_fighter_profile, frLayout, true);

        choosedFighter = getIntent().getParcelableExtra(ArenaFighter.class.getCanonicalName());
        initViews();
        setDataOnUi();
    }

    private void initViews() {
        profileSections = findViewById(R.id.profile);
        profileImage = findViewById(R.id.fighter_icon);
        profileName = findViewById(R.id.fighter_name);
        profileDescription = findViewById(R.id.fighter_description);

        currentHp = findViewById(R.id.value_hp);
        currentArmor = findViewById(R.id.value_armor);
        currentDamage = findViewById(R.id.value_damage);

        reCreate = findViewById(R.id.btn_reCreate);
        reCreate.setOnClickListener(this::setReCreatedDataOnUi);

        select = findViewById(R.id.btn_select);
        select.setOnClickListener(this::selectFighter);

    }

    private void selectFighter(View v) {
        choosedFighter.setArmor(Float.parseFloat(currentArmor.getText().toString()));
        choosedFighter.setDamage(Float.parseFloat(currentDamage.getText().toString()));
        choosedFighter.setHealth(Float.parseFloat(currentHp.getText().toString()));
        goToMainActivity();
    }

    private void goToMainActivity() {
        Intent intentLoadChoosedFighterToMainActivity = new Intent(this,MainActivity.class);
        Bundle choosedFighterBundle = new Bundle();
        choosedFighterBundle.putParcelable(ArenaFighter.class.getCanonicalName(),choosedFighter);
        intentLoadChoosedFighterToMainActivity.putExtras(choosedFighterBundle);
        startActivity(intentLoadChoosedFighterToMainActivity);
    }

    private void setReCreatedDataOnUi(View view) {
        currentHp.setText(String.valueOf(FightersFactory.generateValue(300)));
        currentDamage.setText(String.valueOf(FightersFactory.generateValue(30)));
        currentArmor.setText(String.valueOf(FightersFactory.generatePercentValue()));
    }

    private void setDataOnUi() {
        profileName.setText(choosedFighter.getName());
        profileDescription.setText(choosedFighter.getDescription());
        loadingImage(choosedFighter, profileImage);
        currentHp.setText(String.valueOf(choosedFighter.getHealth()));
        currentDamage.setText(String.valueOf(choosedFighter.getDamage()));
        currentArmor.setText(String.valueOf(choosedFighter.getArmor()));
    }

    private void loadingImage(ArenaFighter fighter, ImageView view) {
        loadingImage(fighter.getImageUrl(), view);
    }


    private void loadingImage(String uri, ImageView view) {
        Glide.with(this)
                .load(uri)
                .into(view);
    }
}

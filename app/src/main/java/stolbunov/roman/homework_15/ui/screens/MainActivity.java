package stolbunov.roman.homework_15.ui.screens;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import stolbunov.roman.homework_15.R;
import stolbunov.roman.homework_15.bl.arenas.BattleArena;
import stolbunov.roman.homework_15.data.cash.RuntimeCash;
import stolbunov.roman.homework_15.data.model.fighters.ArenaFighter;
import stolbunov.roman.homework_15.data.model.fighters.FighterType;
import stolbunov.roman.homework_15.data.model.fighters.FightersFactory;

public class MainActivity extends AppCompatActivity {

    private static final String CURRENT_FIGHTER_KEY = "CURRENT_FIGHTER_KEY";
    private static final String URI_VS_IMAGE =
            "http://static1.comicvine.com/uploads/original/11112/111129141/5440487-1122329314-52705.png";
    private float startHPFighter;
    private float startHPEnemy;


    private RuntimeCash cash = RuntimeCash.getInstance();

    private ImageView profileImage;

    private TextView profileName;
    private TextView profileDescription;

    private TextView currentHp;
    private TextView currentArmor;
    private TextView currentDamage;


    private View progress;
    private View profileSections;

    private Button chooseFighterButton;
    private TextView startNewChange;


    private ArenaFighter currentEnemy;

    private BattleArena arena;

    private FighterCreator fighterCreator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        restoreState(savedInstanceState);
//        setDataOnUi();

    }

    private void setDataOnUi() {
        profileName.setText(cash.getCurrentFIghetr().getName());
        profileDescription.setText(cash.getCurrentFIghetr().getDescription());
        loadingImage(cash.getCurrentFIghetr(), profileImage);
        currentHp.setText(String.valueOf(cash.getCurrentFIghetr().getHealth()));
        currentDamage.setText(String.valueOf(cash.getCurrentFIghetr().getDamage()));
        currentArmor.setText(String.valueOf(cash.getCurrentFIghetr().getArmor()));
    }

    private void restoreState(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            //FirstLaunch
            if (fighterCreator != null && fighterCreator.getStatus() == AsyncTask.Status.FINISHED)
                fighterCreator = null;
            fighterCreator = new FighterCreator();
            fighterCreator.execute(FighterType.KNIGHT);
        } else {
//            restratrt
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(CURRENT_FIGHTER_KEY, cash.getCurrentFIghetr());
    }

    private void startNewFight(View view) {
        startActivityForResult(FightActivity.createFighter(this, FighterType.DRAGON), 1);
    }

    private void addFightResultOnLayout() {
        initViewForInflate();
        fillingView();
//        battleHistory.addView(battleView);
    }

    private void fillingView() {
//        loadingImage(cash.getCurrentFIghetr(), battleImageFighter1);
//        loadingImage(currentEnemy, battleImageFighter2);
//        loadingImage(URI_VS_IMAGE, battleImageVS);
//        battleNameFighter1.setText(cash.getCurrentFIghetr().getName());
//        battleNameFighter2.setText(currentEnemy.getName());
//        battleHPFighter1.setText(String.valueOf(startHPFighter));
//        battleHPFighter2.setText(String.valueOf(startHPEnemy));
//        battleResult.setText(String.format(
//                getResources().getString(R.string.battle_result),
//                currentWinner.getName(),
//                currentWinner.getTotalDamage(),
//                currentWinner.getHealth()));
    }

    private void loadingImage(ArenaFighter fighter, ImageView view) {
        loadingImage(fighter.getImageUrl(), view);
    }


    private void loadingImage(String uri, ImageView view) {
        Glide.with(this)
                .load(uri)
                .into(view);
    }

    private void initViews() {
        profileImage = findViewById(R.id.fighter_icon);

        profileName = findViewById(R.id.fighter_name);
        profileDescription = findViewById(R.id.fighter_description);

        currentHp = findViewById(R.id.value_hp);
        currentArmor = findViewById(R.id.value_armor);
        currentDamage = findViewById(R.id.value_damage);

        startNewChange = findViewById(R.id.btn_start_new_chalange);
        startNewChange.setOnClickListener(this::startNewFight);

        chooseFighterButton = findViewById(R.id.btn_choose_fighter);
        chooseFighterButton.setOnClickListener(this::goToChooseFighterActivity);

        progress = findViewById(R.id.progres_profile);
        profileSections = findViewById(R.id.profile);

        //        battleHistory = findViewById(R.id.battle_history);
    }

    private void goToChooseFighterActivity(View view) {
        Intent intentLoadChooseFighterActivity = new Intent(MainActivity.this,
                ChooseFighterActivity.class);
        startActivity(intentLoadChooseFighterActivity);
    }

    private void initViewForInflate() {
//        LayoutInflater inflater = getLayoutInflater();
//        battleView = inflater.inflate(R.layout.show_battle_fighters, battleHistory, false);
//        battleImageFighter1 = battleView.findViewById(R.id.iv_battle_fighter1);
//        battleImageFighter2 = battleView.findViewById(R.id.iv_battle_fighter2);
//        battleImageVS = battleView.findViewById(R.id.iv_battle_vs);
//        battleNameFighter1 = battleView.findViewById(R.id.tv_name_fighter1);
//        battleNameFighter2 = battleView.findViewById(R.id.tv_name_fighter2);
//        battleHPFighter1 = battleView.findViewById(R.id.tv_hp_fighter1);
//        battleHPFighter2 = battleView.findViewById(R.id.tv_hp_fighter2);
//        battleResult = battleView.findViewById(R.id.tv_battle_result);
    }


    class FighterCreator extends AsyncTask<FighterType, Void, ArenaFighter> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            profileSections.setVisibility(View.GONE);
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArenaFighter doInBackground(FighterType... fighterTypes) {
            ArenaFighter fighter = FightersFactory.generateFighter(fighterTypes[0]);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return fighter;
        }


        @Override
        protected void onPostExecute(ArenaFighter arenaFighter) {
            super.onPostExecute(arenaFighter);
            profileSections.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);
            cash.setCurrentFIghetr(arenaFighter);
            setDataOnUi();
        }
    }
}

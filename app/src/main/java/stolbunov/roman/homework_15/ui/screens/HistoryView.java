package stolbunov.roman.homework_15.ui.screens;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class HistoryView {

    private LinearLayout battleHistory;
    private ImageView battleImageFighter1;
    private ImageView battleImageFighter2;
    private ImageView battleImageVS;
    private TextView battleNameFighter1;
    private TextView battleNameFighter2;
    private TextView battleHPFighter1;
    private TextView battleHPFighter2;
    private TextView battleResult;
    private View battleView;

}

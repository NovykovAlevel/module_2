package stolbunov.roman.homework_15.data.model.history;

import java.util.List;

import stolbunov.roman.homework_15.data.model.fighters.ArenaFighter;


public class FightHistory {

    ArenaFighter firstFighter;
    ArenaFighter SecondFighter;

    List<FightTurn> turns;
    FightResult fightResult;
}

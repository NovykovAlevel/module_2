package stolbunov.roman.homework_15.data.model.fighters;

public interface PostAttackAction {
    void postAttackAction (float damageTaken, float damageGotten);
}

package stolbunov.roman.homework_15.data.model.fighters.magicians;


import android.os.Parcel;

import stolbunov.roman.homework_15.data.model.fighters.ArenaFighter;
import stolbunov.roman.homework_15.data.model.fighters.Elements;
import stolbunov.roman.homework_15.data.model.fighters.FighterType;

public class Mage extends ArenaFighter implements Elements {
    int element;

    public Mage (String name, float health, float damage, float armor, int element, String imageUrl) {
        super(name, health, damage, armor,imageUrl);
        this.element = element;
        classFighter = FighterType.MAGE.name();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(element);
    }

    public Mage(Parcel in) {
        super(in);
        element = in.readInt();
    }

    @Override
    public float attack (ArenaFighter fighters) {
        if( fighters instanceof Elements ) {
            if( isElementsEquals(((Elements) fighters).getElements()) ) {
                return 0;
            }
        }
        return fighters.damaged(damage);
    }

    @Override
    public String getDescription() {
        return "Mage: He is in control of the elements, causing great damage.\n" +
                "He is friends with hobbits ....";
    }

    @Override
    protected int getType() {
        return FighterType.MAGE.getType();
    }

    @Override
    public int getElements () {
        return element;
    }
}

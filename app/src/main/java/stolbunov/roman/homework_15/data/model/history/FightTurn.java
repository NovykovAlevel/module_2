package stolbunov.roman.homework_15.data.model.history;


public class FightTurn {

    float dam1;
    float dam2;

    public FightTurn(float dam1, float dam2) {
        this.dam1 = dam1;
        this.dam2 = dam2;
    }

    public boolean isFinal() {
        return false;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}

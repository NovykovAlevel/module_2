package stolbunov.roman.homework_15.data.model.fighters.knights;

import android.os.Parcel;

import stolbunov.roman.homework_15.data.model.fighters.FighterType;
import stolbunov.roman.homework_15.data.model.fighters.PostAttackAction;

public class BlackKinght extends Knight implements PostAttackAction {

    public BlackKinght(String name, float health, float damage, float armor, float shield, String imageUrl) {
        super(name, health, damage, armor, shield, imageUrl);
        classFighter = FighterType.BLACK_KNIGHT.name();
    }

    public BlackKinght(Parcel in) {
        super(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

    private void recovery(float damageTaken) {
        if (this.isAlfie()) {
            float recovery = damageTaken / 2;
            this.heal(recovery);
            System.out.println(this.getName() + " recovery " + recovery);
        }
    }

    @Override
    public String getDescription() {
        return "Black Knight: It has strong armor and great vitality.\n" +
                "Passive skill: with a certain probability blocks with a shield\n" +
                "incoming damage from the enemy, and also heals himself half of the damage.";
    }

    @Override
    public void postAttackAction(float damageGiven, float damageGotten) {
        recovery(damageGiven);
    }

    @Override
    protected int getType() {
        return FighterType.BLACK_KNIGHT.getType();
    }
}

package stolbunov.roman.homework_15.data.model.fighters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import stolbunov.roman.homework_15.data.model.fighters.dragonRiders.DragonRider;
import stolbunov.roman.homework_15.data.model.fighters.dragons.Dragon;
import stolbunov.roman.homework_15.data.model.fighters.knights.BlackKinght;
import stolbunov.roman.homework_15.data.model.fighters.knights.HollyKnight;
import stolbunov.roman.homework_15.data.model.fighters.knights.Knight;
import stolbunov.roman.homework_15.data.model.fighters.magicians.Mage;

public class FightersFactory {
    static private int counterNames = 0;

    static HashMap<FighterType, String> urlDataBase = new HashMap<>();
    static List<String> listNames = new ArrayList<>();

    static {
        urlDataBase.put(FighterType.DRAGON, "https://dragon-quest.org/images/thumb/4/4c/DQIII_Wyrtle.png/280px-DQIII_Wyrtle.png");
        urlDataBase.put(FighterType.DRAGON_RIDER, "https://i.pinimg.com/originals/59/5e/c6/595ec61c7774c73ded78f8e89d353fa6.png");
        urlDataBase.put(FighterType.KNIGHT, "https://dragon-quest.org/images/thumb/b/b4/DQV_Slime_Knight.png/250px-DQV_Slime_Knight.png");
        urlDataBase.put(FighterType.HOLLY_KNIGHT, "https://cdn2.iconfinder.com/data/icons/fantasy-characters/512/knight2-512.png");
        urlDataBase.put(FighterType.BLACK_KNIGHT, "https://cdn2.iconfinder.com/data/icons/fantasy-characters/512/skeleton2-512.png");
        urlDataBase.put(FighterType.MAGE, "https://cdn2.iconfinder.com/data/icons/fantasy-characters/512/wizard3-512.png");

        listNames.add("Arthur");
        listNames.add("Leonard");
        listNames.add("Sheldon");
        listNames.add("Jack");
        listNames.add("Carl");
        listNames.add("Harry");
        listNames.add("Gregory");
        listNames.add("Richard");
        listNames.add("Lucifer");
        listNames.add("Neo");
        listNames.add("Clark");
        listNames.add("Jon");
        listNames.add("Aria");
        listNames.add("Harry");
        listNames.add("Ron");
        listNames.add("Kokosik");
    }

    static class UnknownFighterException extends RuntimeException {
    }

    public static ArenaFighter generateFighter(FighterType fighterType) {
        return generateFighter(fighterType, generateName());
    }

    public static ArenaFighter generateFighter(FighterType fighterType, String name) {
        switch (fighterType) {
            case DRAGON:
                return new Dragon(name, generateValue(300),
                        generateValue(30),
                        generatePercentValue(), 0,
                        urlDataBase.get(fighterType));
            case KNIGHT:
                return new Knight(name, generateValue(300),
                        generateValue(30),
                        generatePercentValue(),
                        generatePercentValue(),
                        urlDataBase.get(fighterType));
            case DRAGON_RIDER:
                return new DragonRider(name,
                        generateValue(300),
                        generateValue(30),
                        generatePercentValue(),
                        urlDataBase.get(fighterType));
            case HOLLY_KNIGHT:
                return new HollyKnight(name,generateValue(300),
                        generateValue(30),
                        generatePercentValue(),
                        generatePercentValue(),
                        generatePercentValue(),
                        urlDataBase.get(fighterType));
            case BLACK_KNIGHT:
                return new BlackKinght(name, generateValue(300),
                        generateValue(30),
                        generatePercentValue(),
                        generatePercentValue(),
                        urlDataBase.get(fighterType));

            case MAGE:
                return new Mage(name, generateValue(300),
                        generateValue(30),
                        generatePercentValue(),0,
                        urlDataBase.get(fighterType));

        }
        throw new UnknownFighterException();
    }

    public static float generateValue(int max) {
        Random random = new Random();
        return Math.abs(random.nextInt(max));

    }

    public static float generatePercentValue() {
        Random random = new Random();
        return (float) Math.abs(random.nextGaussian());
    }

    private static String generateName() {
        String name;
        if (counterNames < listNames.size()) {
            name = listNames.get(counterNames);
        } else {
            counterNames = 0;
            name = listNames.get(counterNames);
        }
        counterNames++;
        return name;
    }
}

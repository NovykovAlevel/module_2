package stolbunov.roman.homework_15.data.model.fighters.knights;


import android.os.Parcel;

import stolbunov.roman.homework_15.data.model.fighters.FighterType;
import stolbunov.roman.homework_15.data.model.fighters.PostAttackAction;

public class HollyKnight extends Knight implements PostAttackAction {
    private float recovery;

    public HollyKnight(String name, float health, float damage, float armor, float shield, float recovery, String imageUrl) {
        super(name, health, damage, armor, shield, imageUrl);
        this.recovery = recovery;
        classFighter = FighterType.HOLLY_KNIGHT.name();
    }

    public HollyKnight(Parcel in) {
        super(in);
        recovery = in.readFloat();
    }

    private void recovery() {
        if (this.isAlfie()) {
            this.heal(recovery);
            System.out.println(this.getName() + " recovery " + recovery);
        }
    }

    @Override
    public void postAttackAction(float damageTaken, float damageGotten) {
        recovery();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeFloat(recovery);
    }

    @Override
    public String getDescription() {
        return "Holly Knight: It has strong armor and great vitality.\n" +
                "Passive skill: with a certain probability blocks with a shield\n" +
                "incoming damage from the enemy, and also heals himself after each of his attacks.";
    }

    @Override
    protected int getType() {
        return FighterType.HOLLY_KNIGHT.getType();
    }
}